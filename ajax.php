<?php
  require_once('includes/load.php');
  if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
?>

<?php
 // Auto suggetion
    $html = '';
   if(isset($_POST['product_name']) && strlen($_POST['product_name']))
   {
     $products = find_product_by_title($_POST['product_name']);
     if($products){
        foreach ($products as $product):

           $html .= sprintf("<li data-sku='%s' class='list-group-item productForAdd'>%s</li>",$product['id'],$product['name']);
         endforeach;
      } else {

        $html .= '<li onClick=\"fill(\''.addslashes().'\')\" class=\"list-group-item\">';
        $html .= 'No encontrado';
        $html .= "</li>";

      }

      echo json_encode($html);
   }
 ?>

 <?php
  // Auto suggetion
     $html = '';
    if(isset($_POST['corporation_name']) && strlen($_POST['corporation_name']))
    {
      $products = find_sales_by_corporation($_POST['corporation_name']);
      if($products){
         foreach ($products as $product):

            $html .= sprintf("<li  class='list-group-item corpSales'>%s</li>",$product['corporation']);
          endforeach;
       } else {

         $html .= '<li onClick=\"fill(\''.addslashes().'\')\" class=\"list-group-item\">';
         $html .= 'No encontrado';
         $html .= "</li>";

       }

       echo json_encode($html);
    }
  ?>
  <?php
   // Auto suggetion
      $html = '';
     if(isset($_POST['sales_name']) && strlen($_POST['sales_name']))
     {
       $products = find_sales_by_salesman($_POST['sales_name']);
       if($products){
          foreach ($products as $product):

             $html .= sprintf("<li  class='list-group-item salesmanSales'>%s</li>",$product['salesman']);
           endforeach;
        } else {

          $html .= '<li onClick=\"fill(\''.addslashes().'\')\" class=\"list-group-item\">';
          $html .= 'No encontrado';
          $html .= "</li>";

        }

        echo json_encode($html);
     }
   ?>

 <?php
 // find all product
  if(isset($_POST['p_name']) && strlen($_POST['p_name']))
  {
    $product_title = remove_junk($db->escape($_POST['p_name']));
    if($results = find_all_product_info_by_title($product_title)){
        foreach ($results as $result) {

          $html .= "<tr>";

          $html .= "<td id=\"s_name\">".$result['name']."</td>";
          $html .= "<input type=\"hidden\" name=\"s_id\" value=\"{$result['id']}\">";
          $html  .= "<td>";
          $html  .= "<input type=\"text\" class=\"form-control\" name=\"price\" value=\"{$result['sale_price']}\">";
          $html  .= "</td>";
          $html .= "<td id=\"s_qty\">";
          $html .= "<input type=\"text\" class=\"form-control\" name=\"quantity\" value=\"1\">";
          $html  .= "</td>";
          $html  .= "<td>";
          $html  .= "<input type=\"text\" class=\"form-control\" name=\"total\" value=\"{$result['sale_price']}\">";
          $html  .= "</td>";
          $html  .= "<td>";
          $html  .= "<input type=\"date\" class=\"form-control datePicker\" name=\"date\" data-date data-date-format=\"yyyy-mm-dd\">";
          $html  .= "</td>";
          $html  .= "<td>";
          $html  .= "<button type=\"submit\" name=\"add_sale\" class=\"btn btn-primary\">Agregar</button>";
          $html  .= "</td>";
          $html  .= "</tr>";

        }
    } else {
        $html ='<tr><td>El producto no se encuentra registrado en la base de datos</td></tr>';
    }

    echo json_encode($html);
  }
 ?>
