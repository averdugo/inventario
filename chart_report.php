<?php
$page_title = 'Reporte de ventas';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php include_once('layouts/header.php'); ?>
<script src="libs/js/Chart.bundle.js" charset="utf-8"></script>
<script src="libs/js/utils.js" charset="utf-8"></script>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
          <form class="clearfix" method="post" action="sale_report_process.php">
            <div class="form-group">
              <label class="form-label">Rango de fechas</label>
                <div class="input-group">
                  <input type="text" class="datepicker form-control" name="start-date" placeholder="Desde">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-menu-right"></i></span>
                  <input type="text" class="datepicker form-control" name="end-date" placeholder="Hasta">
                </div>
            </div>
            <div class="form-group">
                 <button type="submit" name="submit" class="btn btn-primary">Generar Graficos</button>
            </div>
          </form>
          <div class="row">
              <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          Venta Mensual
                      </div>
                      <div class="panel-body">
                          <canvas id="canvas"></canvas>
                      </div>
                  </div>
              </div>
              <script>
                  var config = {
                      type: 'line',
                      data: {
                          labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Diciembre"],
                          datasets: [ {
                              label: "Ventas:$ ",
                              backgroundColor: window.chartColors.blue,
                              borderColor: window.chartColors.blue,
                              data: [
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor(),
                                  randomScalingFactor()
                              ],
                              fill: false,
                          }]
                      },
                      options: {
                          responsive: true,
                          title:{
                              display:true,
                              text:'Venta Mensual'
                          },
                          tooltips: {
                              mode: 'index',
                              intersect: false,
                          },
                          hover: {
                              mode: 'nearest',
                              intersect: true
                          },
                          scales: {
                              xAxes: [{
                                  display: true,
                                  scaleLabel: {
                                      display: true,
                                      labelString: 'Mes'
                                  }
                              }],
                              yAxes: [{
                                  display: true,
                                  scaleLabel: {
                                      display: true,
                                      labelString: 'Valor'
                                  }
                              }]
                          }
                      }
                  };


              </script>
              <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          Venta Mensual
                      </div>
                      <div class="panel-body">
                          <canvas id="chart-area" />
                      </div>
                      <script>
                      var randomScalingFactor = function() {
                          return Math.round(Math.random() * 100);
                      };

                      var config2 = {
                          type: 'pie',
                          data: {
                              datasets: [{
                                  data: [
                                      randomScalingFactor(),
                                      randomScalingFactor(),
                                      randomScalingFactor(),
                                      randomScalingFactor(),
                                      randomScalingFactor(),
                                  ],
                                  backgroundColor: [
                                      window.chartColors.red,
                                      window.chartColors.orange,
                                      window.chartColors.yellow,
                                      window.chartColors.green,
                                      window.chartColors.blue,
                                  ],
                                  label: 'Dataset 1'
                              }],
                              labels: [
                                  "Producto 1",
                                  "Producto 2",
                                  "Producto 3",
                                  "Producto 4",
                                  "Producto 5"
                              ]
                          },
                          options: {
                              responsive: true
                          }
                      };

                      window.onload = function() {
                          var ctx2 = document.getElementById("chart-area").getContext("2d");
                          window.myPie = new Chart(ctx2, config2);

                          var ctx = document.getElementById("canvas").getContext("2d");
                          window.myLine = new Chart(ctx, config);
                      };

                      </script>
                  </div>
              </div>
          </div>
      </div>

    </div>
  </div>

</div>
<?php include_once('layouts/footer.php'); ?>
