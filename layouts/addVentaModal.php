<!-- Modal -->
<div class="modal fade" id="addVentaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Venta del Producto <span id="pName"></span></h4>
      </div>
      <div class="modal-body">
		  <form class="" action="add_sale.php" method="post">
			  <div class="form-group">
                <div class="row">
                    <input type="hidden" name="s_id" id="saleId" value="">
					<div class="col-md-6">
						<label for="">Fecha</label>
						<input type="date" class="form-control" name="date" placeholder="">
                  	</div>
                    <div class="col-md-6">
						<label for="">Empresa</label>
						<input type="text" class="form-control" name="corporation" placeholder="">
                  	</div>
                    <div class="col-md-6">
						<label for="">Vendedor</label>
						<input type="text" class="form-control" name="salesman" placeholder="">
                  	</div>
                  	<div class="col-md-6">
						<label for="">Cantidad</label>
						<input type="number" class="form-control" name="quantity" placeholder="">
                  	</div>
					<div class="col-md-6">
						<label for="">Total</label>
						<input type="number" class="form-control" name="total" placeholder="">
                  	</div>
                </div>
              </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" name="add_sale">Guardar</button>
      </div>
	  </form>
    </div>
  </div>
</div>
