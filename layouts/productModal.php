<!-- Modal -->
<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Seleccione Consignación</h4>
        </div>
      	<div class="modal-body">
		  	<div class="row">
				<?php foreach ($all_categories = find_all('categories') as $k => $v): ?>
	  			  	<div class="col-md-6" style="margin-bottom:20px">
					  	<a href="product.php?cc=<?php echo $v['id']; ?>">
						  	<button type="button" class="btn btn-primary btn-block" name="button"><?php echo $v['name']; ?></button>
					  	</a>
	  			  	</div>
	  		  	<?php endforeach; ?>
		  	</div>
      	</div>
    </div>
  </div>
</div>
