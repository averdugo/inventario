
function suggetion() {

     $('#sug_input1').keyup(function(e) {
         
         var formData = {
             'product_name' : $('input[name=title]').val()
         };

         if(formData['product_name'].length >= 1){

           // process the form
           $.ajax({
               type        : 'POST',
               url         : 'ajax.php',
               data        : formData,
               dataType    : 'json',
               encode      : true
           })
               .done(function(data) {
                   //console.log(data);
                   $('#result1').html(data).fadeIn();
                   $('#result1 li').click(function() {

                     $('#sug_input1').val($(this).text());
                     $('#result1').fadeOut(500);

                   });

                   $("#sug_input1").blur(function(){
                     $("#result1").fadeOut(500);
                   });

               });

         } else {

           $("#result1").hide();

         };

         e.preventDefault();
     });

 }

 function suggetion2() {

      $('#sug_input2').keyup(function(e) {

          var formData = {
              'corporation_name' : $('input[name=corporation]').val()
          };

          if(formData['corporation_name'].length >= 1){

            // process the form
            $.ajax({
                type        : 'POST',
                url         : 'ajax.php',
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
                .done(function(data) {
                    //console.log(data);
                    $('#result2').html(data).fadeIn();
                    $('#result2 li').click(function() {

                      $('#sug_input2').val($(this).text());
                      $('#result2').fadeOut(500);

                    });

                    $("#sug_input2").blur(function(){
                      $("#result2").fadeOut(500);
                    });

                });

          } else {

            $("#result2").hide();

          };

          e.preventDefault();
      });

  }
  function suggetion3() {

       $('#sug_input3').keyup(function(e) {

           var formData = {
               'sales_name' : $('input[name=salesman]').val()
           };

           if(formData['sales_name'].length >= 1){

             // process the form
             $.ajax({
                 type        : 'POST',
                 url         : 'ajax.php',
                 data        : formData,
                 dataType    : 'json',
                 encode      : true
             })
                 .done(function(data) {
                     //console.log(data);
                     $('#result3').html(data).fadeIn();
                     $('#result3 li').click(function() {

                       $('#sug_input3').val($(this).text());
                       $('#result3').fadeOut(500);

                     });

                     $("#sug_input3").blur(function(){
                       $("#result3").fadeOut(500);
                     });

                 });

           } else {

             $("#result3").hide();

           };

           e.preventDefault();
       });

   }

  $('#sug-form').submit(function(e) {
      var formData = {
          'p_name' : $('input[name=title]').val()
      };
        // process the form
        $.ajax({
            type        : 'POST',
            url         : 'ajax.php',
            data        : formData,
            dataType    : 'json',
            encode      : true
        })
            .done(function(data) {
                //console.log(data);
                $('#product_info').html(data).show();
                total();
                $('.datePicker').datepicker('update', new Date());

            }).fail(function() {
                $('#product_info').html(data).show();
            });
      e.preventDefault();
  });
  function total(){
    $('#product_info input').change(function(e)  {
            var price = +$('input[name=price]').val() || 0;
            var qty   = +$('input[name=quantity]').val() || 0;
            var total = qty * price ;
                $('input[name=total]').val(total.toFixed(2));
    });
  }

  $(document).ready(function() {

    //tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $('.submenu-toggle').click(function () {
       $(this).parent().children('ul.submenu').toggle(200);
    });
    //suggetion for finding product names
    suggetion();
    suggetion2();
    suggetion3();
    // Callculate total ammont
    total();

    $('body').on('click','.productForAdd',function(){
        var pName = $(this).text();
        var pSku = $(this).data('sku')
        $('#pName').html(pName);
        $('#saleId').val(pSku);
        $('#addVentaModal').modal()

    })
    $('body').on('click','.salesmanSales',function(){
        var name= $(this).text();
        var cId = $('#cId').val();
        $.get('sales_data.php?name='+name+'&search=salesman&categorie='+cId,function(r){
            $('#tableDataSales').html(r);
        })
    })
    $('body').on('click','.corpSales',function(){
        var name= $(this).text();
        var cId = $('#cId').val();
        $.get('sales_data.php?name='+name+'&search=corporation&categorie='+cId,function(r){
            $('#tableDataSales').html(r);
        })
    })

    $('.datepicker')
        .datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
  });
