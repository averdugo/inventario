<?php
require_once __DIR__ . '/includes/simplexlsx.class.php';
require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}

/*
$target_dir = 'libs/excel/';
$target_file = $target_dir . basename($_FILES["file_upload"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file);*/



$xlsx = SimpleXLSX::parse( $_FILES['file_upload']['tmp_name'] );

foreach ($xlsx->rows() as $key => $value) {

	if ($key != 0) {

		$product = find_product_by_sku($value[0]);

		$p_id      = $db->escape((int)$product[0]['id']);
		$s_qty     = $db->escape((int)$value[1]);
		$s_total   = $db->escape($value[2]);
		$date      = $db->escape($value[3]);
		$s_date    = make_date();

		$sql  = "INSERT INTO sales (";
		$sql .= " product_id,qty,price,date";
		$sql .= ") VALUES (";
		$sql .= "'{$p_id}','{$s_qty}','{$s_total}','{$s_date}'";
		$sql .= ")";

		$db->query($sql);
		update_product_qty($s_qty,$p_id);
	}

}
header("Location: sales.php");
die();


?>
