<?php
  $page_title = 'Lista de ventas';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(3);
?>
<?php
    $sales = find_all_sale_cc($_GET['cc']);
    $categorie = find_by_id('categories',(int)$_GET['cc']);
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
    <input type="hidden" id="cId" value="<?php echo $_GET['cc']; ?>">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
  </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <strong>
            <span class="glyphicon glyphicon-th"></span>
            <span>Todas la ventas <?php echo $categorie['name']; ?></span><br><br>
                <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-primary">Búsqueda</button>
                        </span>
                        <input type="text" id="sug_input2" class="form-control" name="corporation"  placeholder="Buscar por Empresa">
                     </div>
                     <div id="result2" class="list-group"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-primary">Búsqueda</button>
                        </span>
                        <input type="text" id="sug_input3" class="form-control" name="salesman"  placeholder="Buscar por Vendedor">
                     </div>
                     <div id="result3" class="list-group"></div>
                    </div>
                </div>
                

          </strong>
          <div class="pull-right">
            <a href="add_sale.php" class="btn btn-primary">Agregar venta</a>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" style="width: 15%;"> Fecha </th>
                <th class="text-center" style="width: 15%;"> Empresa </th>
                <th style="width: 15%;"> Producto </th>
                <th class="text-center" style="width: 15%;"> Vendedor </th>
                <th class="text-center" style="width: 15%;"> Cantidad</th>
                <th class="text-center" style="width: 15%;"> Total </th>
                <th class="text-center" style="width: 100px;"> Acciones </th>
             </tr>
            </thead>
           <tbody id="tableDataSales">
             <?php foreach ($sales as $sale):?>

             <tr>
               <td class="text-center"><?php echo $sale['date']; ?></td>
               <td><?php echo remove_junk($sale['corporation']); ?></td>
               <td><?php echo remove_junk($sale['name']); ?></td>
               <td><?php echo remove_junk($sale['salesman']); ?></td>
               <td class="text-center"><?php echo (int)$sale['qty']; ?></td>
               <td class="text-center"><?php echo remove_junk($sale['price']); ?></td>

               <td class="text-center">
                  <div class="btn-group">
                     <a href="edit_sale.php?id=<?php echo (int)$sale['id'];?>" class="btn btn-warning btn-xs"  title="Edit" data-toggle="tooltip">
                       <span class="glyphicon glyphicon-edit"></span>
                     </a>
                     <a href="delete_sale.php?id=<?php echo (int)$sale['id'];?>" class="btn btn-danger btn-xs"  title="Delete" data-toggle="tooltip">
                       <span class="glyphicon glyphicon-trash"></span>
                     </a>
                  </div>
               </td>
             </tr>
             <?php endforeach;?>
           </tbody>
         </table>
        </div>
      </div>
    </div>
  </div>
<?php include_once('layouts/footer.php'); ?>
